﻿using System;
using System.Collections.Generic;
using MovieCharacterApi.Models.DTO.Movies;

namespace MovieCharacterApi.Models.DTOs.Characters
{
    public class CharactersReadDTO
    {
        public int CharacterId { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureLink { get; set; }
        public List<int> MovieId { get; set; }
        public List<string> MovieName { get; set; }

    }
}
