﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Models.DTO.Franchises
{
    public class FranchisesEditDTO
    {
        public int FranchiseId { get; set; }
        public string FranchiseName { get; set; }
        public string Description { get; set; }

    }
}
