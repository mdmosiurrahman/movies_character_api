﻿using MovieCharacterApi.Models.DTO.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Models.DTO.Franchises
{
    public class FranchisesReadDTO
    {

        public int FranchiseId { get; set; }
        public string FranchiseName { get; set; }
        public string Description { get; set; }

        //Relational Props
        public List<int> MovieId { get; set; }
        public List<string> MovieName { get; set; }
    }
}
