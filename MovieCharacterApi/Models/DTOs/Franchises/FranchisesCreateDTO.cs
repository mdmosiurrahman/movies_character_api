﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Models.DTO.Franchises
{
    public class FranchisesCreateDTO
    {

        public string FranchiseName { get; set; }
        public string Description { get; set; }

        public List<int> Movies { get; set; }
    }
}
