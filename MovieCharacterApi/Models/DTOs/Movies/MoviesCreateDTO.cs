﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Models.DTO.Movies
{
    public class MoviesCreateDTO
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string TrailerLink { get; set; }

        public int FranchiseId { get; set; }
    }
}
