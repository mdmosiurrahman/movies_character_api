﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Models
{
    public class MovieCharacterDbContext : DbContext
    {
        public MovieCharacterDbContext( DbContextOptions options) : base(options)
        {
        }

        public DbSet<Movies> Movies{ get; set; }
        public DbSet<Characters> Characters { get; set; }
        public DbSet<Franchises> Franchises { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchises>().HasData(new Franchises { FranchiseId = 1, FranchiseName = "Paramount", Description = "World famous movie franchiser " });
            modelBuilder.Entity<Franchises>().HasData(new Franchises { FranchiseId = 2, FranchiseName = "Star group", Description = "Big company " });


            modelBuilder.Entity<Characters>().HasData(new Characters { CharacterId = 1, FullName = "Superman", Alias = "Power with S", Gender = "Male", PictureLink = "https://i.pinimg.com/originals/15/cb/f5/15cbf5fb4f4449569815e9267f9e6d3c.jpg" });
            modelBuilder.Entity<Characters>().HasData(new Characters { CharacterId = 2, FullName = "Batman", Alias = "Power with B", Gender = "Male", PictureLink = "https://i.pinimg.com/originals/15/cb/f5/15cbf5fb4f4449569815e9267f9e6d3c.jpg" });
            modelBuilder.Entity<Characters>().HasData(new Characters { CharacterId = 3, FullName = "Spiderman", Alias = "Power with Net", Gender = "Male", PictureLink = "https://i.pinimg.com/originals/15/cb/f5/15cbf5fb4f4449569815e9267f9e6d3c.jpg" });

            modelBuilder.Entity<Movies>().HasData(new Movies { MovieId = 1, Title = "Superman on Earth", ReleaseYear = 2019, Director = "Mosiur", FranchiseId = 1 });
            modelBuilder.Entity<Movies>().HasData(new Movies { MovieId = 2, Title = "Batman Return", ReleaseYear = 2018, Director = "Rahman", FranchiseId = 2 });
            modelBuilder.Entity<Movies>().HasData(new Movies { MovieId = 3, Title = "Spiderman on Rise", ReleaseYear = 2017, Director = "Sohel", FranchiseId = 1 });  
            

            modelBuilder.Entity<Characters>()
                .HasMany(x => x.Movies)
                .WithMany(y => y.Characters)
                .UsingEntity<Dictionary<string, object>>(
                "CharaterMovie",
                p => p.HasOne<Movies>().WithMany().HasForeignKey("MovieId"),
                m => m.HasOne<Characters>().WithMany().HasForeignKey("CharacterId"),
                n =>
                {
                    n.HasKey("CharacterId", "MovieId");
                    n.HasData(
                        new { MovieId = 1, CharacterId = 2 },
                        new { MovieId = 1, CharacterId = 3 },
                        new { MovieId = 3, CharacterId = 1 },
                        new { MovieId = 2, CharacterId = 2 },
                        new { MovieId = 2, CharacterId = 3 }

                       );
                });
        }
    }
}
