﻿using AutoMapper;
using MovieCharacterApi.Models;
using MovieCharacterApi.Models.DTO.Franchises;
using System.Linq;

namespace MovieCharacterApi.Profiles
{
    public class FrachisesProfile : Profile
    {
        public FrachisesProfile()
        {
            CreateMap<Franchises, FranchisesReadDTO>()
           .ForMember(fdto => fdto.MovieId, opt => opt
             .MapFrom(f => f.Movies.Select(m => m.MovieId).ToArray()))
             .ForMember(fdto => fdto.MovieName, opt => opt
             .MapFrom(c => c.Movies.Select(m => m.Title).ToArray())).
           ReverseMap();

            CreateMap<Franchises, FranchisesCreateDTO>().ReverseMap();
            CreateMap<Franchises, FranchisesEditDTO>().ReverseMap();
        }
    }
}
