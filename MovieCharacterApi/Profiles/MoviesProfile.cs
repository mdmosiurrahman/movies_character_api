﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MovieCharacterApi.Models;
using MovieCharacterApi.Models.DTO.Movies;

namespace MovieCharacterApi.Profiles
{
    public class MoviesProfile : Profile
    {
        public MoviesProfile()
        {
            CreateMap<Movies, MoviesReadDTO>()
            .ForMember(mdto => mdto.FranchiseId, opt => opt
             .MapFrom(m => m.FranchiseId))
          .ForMember(mdto => mdto.Characters, opt => opt
              .MapFrom(m => m.Characters.Select(c => c.CharacterId).ToArray()))
            .ReverseMap();

            CreateMap<Movies, MoviesCreateDTO>()
            .ForMember(mdto => mdto.FranchiseId, opt => opt
                  .MapFrom(m => m.FranchiseId))
                 .ReverseMap();

            CreateMap<Movies, MoviesEditDTO>()
            .ForMember(mdto => mdto.FranchiseId, opt => opt
             .MapFrom(m => m.FranchiseId))
            .ReverseMap();
        }
    }
}
