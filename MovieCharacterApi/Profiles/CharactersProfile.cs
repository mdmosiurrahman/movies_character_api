﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MovieCharacterApi.Models;
using MovieCharacterApi.Models.DTO.Movies;
using MovieCharacterApi.Models.DTOs.Characters;

namespace MovieCharacterApi.Profiles
{
    public class CharactersProfile : Profile
    {
        public CharactersProfile()
        {
            CreateMap<Characters, CharactersReadDTO>()
            .ForMember(cdto => cdto.MovieId, opt => opt
              .MapFrom(c => c.Movies.Select(m => m.MovieId).ToArray()))
              .ForMember(cdto => cdto.MovieName, opt => opt
              .MapFrom(c => c.Movies.Select(m => m.Title).ToArray())).
            ReverseMap();


            CreateMap<Characters, CharactersCreateDTO>().ReverseMap();


            CreateMap<Characters, CharactersEditDTO>().ReverseMap();
            /*.ForMember(cdto => cdto.Movies, opt => opt
                  .MapFrom(m => m.Movies.Select(c => c.MovieId).ToArray()))
                  .ReverseMap();*/
        }
    }
}
