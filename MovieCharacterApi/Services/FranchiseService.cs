﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieCharacterDbContext _context;

        public FranchiseService(MovieCharacterDbContext context)
        {
            _context = context;
        }

        public async Task<Franchises> AddFranchiseAsync(Franchises franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(f => f.FranchiseId == id);
        }

        public async Task<IEnumerable<Franchises>> GetAllFranchisesAsync()
        {
            return await _context.Franchises
                 .Include(f => f.Movies)
                 .ToListAsync();
        }

        public async Task<Franchises> GetSpecificFranchiseAsync(int id)
        {
            Franchises franchise = await _context.Franchises
                           .Include(f => f.Movies)
                           .Where(f => f.FranchiseId == id)
                           .FirstAsync();
            return franchise;
        }

        public async Task UpdateFranchiseAsync(Franchises franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateFranchiseMoviesrAsync(int FranchiseId, List<int> Movies)
        {
            Franchises UpdateFranchiseMovies = await _context.Franchises
               .Include(f => f.Movies)
               .Where(m => m.FranchiseId == FranchiseId)
               .FirstAsync();
            List<Movies> movies = new();
            foreach (int movieId in Movies)
            {
                Movies mov = await _context.Movies.FindAsync(movieId);
                if (mov == null)
                    throw new KeyNotFoundException();
                movies.Add(mov);
            }
            UpdateFranchiseMovies.Movies = movies;
            await _context.SaveChangesAsync();
        }
    }
}
