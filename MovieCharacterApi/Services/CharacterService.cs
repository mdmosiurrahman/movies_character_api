﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly MovieCharacterDbContext _context;

        public CharacterService(MovieCharacterDbContext context)
        {
            _context = context;
        }

        public async Task<Characters> AddCharacterAsync(Characters character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(c => c.CharacterId == id);
        }

        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

        }

        public async Task<IEnumerable<Characters>> GetAllCharactersAsync()
        {
            return await _context.Characters
                           .Include(c => c.Movies)
                           .ToListAsync();
        }

        public async Task<Characters> GetSpecificCharacterAsync(int id)
        {
            //return await _context.Characters.FindAsync(id);
            Characters character = await _context.Characters
               .Include(c => c.Movies)
               .Where(m => m.CharacterId == id)
               .FirstAsync();
            return character;
        }

        public async Task UpdateCharacterAsync(Characters character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCharacterMovieAsync(int CharacterId, List<int> Movies)
        {
            Characters characterToUpdateMovies = await _context.Characters
               .Include(c => c.Movies)
               .Where(m => m.CharacterId == CharacterId)
               .FirstAsync();
            List<Movies> movies = new();
            foreach (int movieId in Movies)
            {
                Movies mov = await _context.Movies.FindAsync(movieId);
                if (mov == null)
                    throw new KeyNotFoundException();
                movies.Add(mov);
            }
            characterToUpdateMovies.Movies = movies;
            await _context.SaveChangesAsync();
        }
    }
}
