﻿using MovieCharacterApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchises>> GetAllFranchisesAsync();
        public Task<Franchises> GetSpecificFranchiseAsync(int id);
        public Task<Franchises> AddFranchiseAsync(Franchises franchise);
        public Task UpdateFranchiseAsync(Franchises franchise);
        public Task UpdateFranchiseMoviesrAsync(int FranchiseId, List<int> Movies);

        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);

    }
}
