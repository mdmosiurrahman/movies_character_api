﻿using MovieCharacterApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharacterApi.Models.DTO.Movies;

namespace MovieCharacterApi.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movies>> GetAllMoviesAsync();
        public Task<Movies> GetSpecificMovieAsync(int id);
        public Task<Movies> AddMovieAsync(Movies movie);
        public Task UpdateMovieAsync(Movies movie);
        public Task UpdateMovieCharacterAsync(int MovieId, List<int> Characters);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
