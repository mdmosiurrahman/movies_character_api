﻿using MovieCharacterApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Services
{
    public interface ICharacterService
    {
        public Task<IEnumerable<Characters>> GetAllCharactersAsync();
        public Task<Characters> GetSpecificCharacterAsync(int id);
        public Task<Characters> AddCharacterAsync(Characters character);
        public Task UpdateCharacterAsync(Characters character);
        public Task UpdateCharacterMovieAsync(int CharacterId, List<int> Movies);
        public Task DeleteCharacterAsync(int id);
        public bool CharacterExists(int id);
    }
}
