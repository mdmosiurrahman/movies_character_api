﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterApi.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieCharacterDbContext _context;

        public MovieService(MovieCharacterDbContext context)
        {
            _context = context;
        }

        public async Task<Movies> AddMovieAsync(Movies movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;

        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Movies>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Include(m => m.Franchise)
                .ToListAsync();
        }

        public async Task<Movies> GetSpecificMovieAsync(int id)
        {
            Movies movie = await _context.Movies
                           .Include(m => m.Characters)
                           .Where(m => m.MovieId == id)
                           .FirstAsync();
            return movie;
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(m => m.MovieId == id);
        }

        public async Task UpdateMovieAsync(Movies movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMovieCharacterAsync(int MovieId, List<int> characters)
        {
            Movies movieToUpdateCharacters = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.MovieId == MovieId)
                .FirstAsync();
            List<Characters> chars = new();
            foreach (int charId in characters)
            {
                Characters charact = await _context.Characters.FindAsync(charId);
                if (charact == null)
                    throw new KeyNotFoundException();
                chars.Add(charact);
            }
            movieToUpdateCharacters.Characters = chars;
            await _context.SaveChangesAsync();
        }

    }
}
