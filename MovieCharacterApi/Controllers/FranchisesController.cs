﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterApi.Models;
using MovieCharacterApi.Models.DTO.Franchises;
using MovieCharacterApi.Services;

namespace MovieCharacterApi.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieCharacterDbContext context, IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _context = context;
            _mapper = mapper;
        }


        // GET: api/Franchises
        /// <summary>
        /// Get all Franchises
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchisesReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchisesReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        // GET: api/Franchises/5
        /// <summary>
        /// Get specific franchises by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchisesReadDTO>> GetFranchises(int id)
        {
            Franchises franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<FranchisesReadDTO>(franchise);
        }


        /// <summary>
        /// Update franchises by Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoFranchise"></param>
        /// <returns></returns>

        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchises(int id, FranchisesEditDTO dtoFranchise)
        {

            if (id != dtoFranchise.FranchiseId)
            {
                return BadRequest();
            }
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            //Map to domain
            Franchises domainFranchise = _mapper.Map<Franchises>(dtoFranchise);
            await _franchiseService.AddFranchiseAsync(domainFranchise);

            return NoContent();
        }

        // POST: api/Franchises
        /// <summary>
        /// add a new franchises
        /// </summary>
        /// <param name="dtoFranchise"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Franchises>> PostFranchises(FranchisesCreateDTO dtoFranchise)
        {
            Franchises domainFranchise = _mapper.Map<Franchises>(dtoFranchise);
            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);
            return CreatedAtAction("GetFranchises", new { FranchiseId = domainFranchise.FranchiseId },
                  _mapper.Map<FranchisesCreateDTO>(dtoFranchise));
        }


        /// <summary>
        /// delete franchises by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchises(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }
        /// <summary>
        /// Update Movies in a given Franchise

        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>

        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMovieFranchises(int id, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            try
            {
                await _franchiseService.UpdateFranchiseMoviesrAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalied Movie.");
            }
            return NoContent();

        }
    }
}
