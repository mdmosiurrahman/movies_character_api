﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterApi.Models;
using MovieCharacterApi.Models.DTO.Movies;
using MovieCharacterApi.Services;

namespace MovieCharacterApi.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MoviesController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;


        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;


        }

        // GET: api/Movies
        /// <summary>
        /// Get all movies
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        public async Task<ActionResult<IEnumerable<MoviesReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MoviesReadDTO>>(await _movieService.GetAllMoviesAsync());

        }

        // GET: api/Movies/5
        /// <summary>
        /// Get specific movie by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MoviesReadDTO>> GetMovies(int id)
        {
            Movies movie = await _movieService.GetSpecificMovieAsync(id);
            if (movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<MoviesReadDTO>(movie);
        }

        // PUT: api/Movies/5
        /// <summary>
        /// update movies
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovies(int id, MoviesEditDTO dtoMovie)
        {
            if (id != dtoMovie.MovieId)
            {
                return BadRequest();
            }
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            //Map to domain
            Movies domainMovie = _mapper.Map<Movies>(dtoMovie);

            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();
        }

        // POST: api/Movies
        /// <summary>
        /// add a new movie
        /// </summary>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movies>> PostMovies(MoviesCreateDTO dtoMovie)
        {
            Movies domainMovie = _mapper.Map<Movies>(dtoMovie);
            domainMovie = await _movieService.AddMovieAsync(domainMovie);
            return CreatedAtAction("GetMovies", new { MovieId = domainMovie.MovieId },
                  _mapper.Map<MoviesCreateDTO>(dtoMovie));
        }

        // DELETE: api/Movies/5
        /// <summary>
        /// Delete movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovies(int id)
        {

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }
        /// <summary>
        /// Updates the movie with Charecters
        /// </summary>
        /// <param name="id"></param>
        /// <param name="charactrers"></param>
        /// <returns></returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> charactrers)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            try
            {
                await _movieService.UpdateMovieCharacterAsync(id, charactrers);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalied character.");
            }
            return NoContent();

        }

    }
}
