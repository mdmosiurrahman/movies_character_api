﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterApi.Models;
using AutoMapper;

using MovieCharacterApi.Models.DTOs.Characters;
using MovieCharacterApi.Services;

namespace MovieCharacterApi.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class CharactersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;
        public CharactersController(ICharacterService characterService, IMapper mapper)
        {

            _characterService = characterService;
            _mapper = mapper;

        }

        // GET: api/Characters
        /// <summary>
        /// get all Characters
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharactersReadDTO>>> GetCharacters()
        {
            //return await _context.Characters.ToListAsync();
            return _mapper.Map<List<CharactersReadDTO>>(await _characterService.GetAllCharactersAsync());
        }

        // GET: api/Characters/5
        /// <summary>
        /// get all Character bt Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharactersReadDTO>> GetCharacters(int id)
        {
            var characters = await _characterService.GetSpecificCharacterAsync(id);


            if (characters == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharactersReadDTO>(characters);
        }

        // PUT: api/Characters/5
        /// <summary>
        /// update characters
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoCharacter"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacters(int id, CharactersEditDTO dtoCharacter)
        {
            if (id != dtoCharacter.CharacterId)
            {
                return BadRequest();
            }

            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            Characters domainCharacter = _mapper.Map<Characters>(dtoCharacter);
            await _characterService.UpdateCharacterAsync(domainCharacter);
            return NoContent();
        }

        // POST: api/Characters
        /// <summary>
        /// add character
        /// </summary>
        /// <param name="dtoCharacter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Characters>> PostCharacters(CharactersCreateDTO dtoCharacter)
        {
            Characters domainCharacter = _mapper.Map<Characters>(dtoCharacter);
            domainCharacter = await _characterService.AddCharacterAsync(domainCharacter);

            return CreatedAtAction("GetCharacters",
                new { CharacterId = domainCharacter.CharacterId },
                 _mapper.Map<CharactersEditDTO>(domainCharacter));
        }



        /// <summary>
        /// deelete character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacters(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            await _characterService.DeleteCharacterAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Updates the movie list for given character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateCharacterMovies(int id, List<int> movies)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            try
            {
                await _characterService.UpdateCharacterMovieAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid MovieId");
            }
            return NoContent();
        }
    }
}
