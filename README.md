### Movie Character API
- Use Entity Framework Code First to create Database 
- Create a Web API in ASP.Net Core
#### Group- Mosiur Rahman & Ramesh Pagilla

### Technologies
- C#, ASP.Net Core, Entity Framework,REST API,SQL Server,Swagger documentation


### Running Code
- assuming that  you have  installed Visual Studio 2019 or later,SQL Server Management Studio .

- Clone project


- Open the project in VS 
- clone add db file to SQL Server
- Open Program.cs and click run


